#include "NewScene2.h"
#include "NewScene.h"
#include "NewScene3.h"
#include "HelloWorldScene.h"

USING_NS_CC;
unsigned int score;
unsigned int n1, n2, n3, n4, n5;
Scene* NewScene3::createScene(unsigned int tempScore)
{
	score = tempScore;
	auto scene = Scene::create();
	auto layer = NewScene3::create();
	scene->addChild(layer);
	return scene;
}

bool NewScene3::init()
{

	if (!Scene::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();
	UserDefault *def = UserDefault::getInstance();
	auto HighScore = def->getIntegerForKey("HIGHSCORE", 0);
	auto HighScore1 = def->getIntegerForKey("HIGHSCORE1", 0);
	auto HighScore2 = def->getIntegerForKey("HIGHSCORE2", 0);
	auto HighScore3 = def->getIntegerForKey("HIGHSCORE3", 0);
	auto HighScore4 = def->getIntegerForKey("HIGHSCORE4", 0);
	if (score > HighScore)
	{
		HighScore = score;
		def->setIntegerForKey("HIGHSCORE", HighScore);
		def->flush();
	}
	else if (score > HighScore1)
	{
		HighScore1 = score;
		def->setIntegerForKey("HIGHSCORE1", HighScore1);
		def->flush();
	}
	else if (score > HighScore2)
	{
		HighScore2 = score;
		def->setIntegerForKey("HIGHSCORE2", HighScore2);
		def->flush();
	}
	else if (score > HighScore3)
	{
		HighScore3 = score;
		def->setIntegerForKey("HIGHSCORE3", HighScore3);
		def->flush();
	}
	else if (score > HighScore4)
	{
		HighScore4 = score;
		def->setIntegerForKey("HIGHSCORE4", HighScore4);
		def->flush();
	}
	n1 = HighScore;
	n2 = HighScore1;
	n3 = HighScore2;
	n4 = HighScore3;
	n5 = HighScore4;
	{
		auto label = Label::createWithSystemFont("Twoj wynik:", "Arial", 20);
		label->setPosition(visibleSize.width *0.25, (visibleSize.height / 7) * 5);
		this->addChild(label);
	}
	{
		auto label = Label::createWithSystemFont("Najlepszy wynik:", "Arial", 20);
		label->setPosition(visibleSize.width *0.75, (visibleSize.height / 7) * 5);
		this->addChild(label);
	}
	{
		auto label = Label::createWithSystemFont(std::to_string(HighScore), "Arial", 20);
		label->setPosition(visibleSize.width *0.75, (visibleSize.height / 7) * 4);
		this->addChild(label);
	}
	{
		auto label = Label::createWithSystemFont(std::to_string(score), "Arial", 20);
		label->setPosition(visibleSize.width *0.25, (visibleSize.height / 7) * 4);
		this->addChild(label);
	}

	auto menu_item_1 = MenuItemFont::create("Jeszcze raz", CC_CALLBACK_1(NewScene3::Again, this));
	auto menu_item_2 = MenuItemFont::create("Menu", CC_CALLBACK_1(NewScene3::Menu, this));
	auto menu_item_3 = MenuItemFont::create("GAME OVER", CC_CALLBACK_1(NewScene3::End, this));

	menu_item_1->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 7) * 2));
	menu_item_2->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 7) * 3));
	menu_item_3->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 7) * 6));

	auto *menu = Menu::create(menu_item_1, menu_item_2,menu_item_3, NULL);
	menu->setPosition(Point(0, 0));
	this->addChild(menu);
	return true;
}
void NewScene3::Again(cocos2d::Ref *pSender)
{
	auto scene = HelloWorld::createScene();
	Director::getInstance()->replaceScene(scene);
}

void NewScene3::Menu(cocos2d::Ref *pSender)
{
	Director::getInstance()->popScene();
}
void NewScene3::End(cocos2d::Ref *pSender)
{
	//auto scene = NewScene2::createScene(n1,n2,n3,n4,n5);
	//Director::getInstance()->replaceScene(scene);
}