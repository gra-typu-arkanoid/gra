#ifndef __NEW_SCENE4_H__
#define __NEW_SCENE4_H__

#include "cocos2d.h"

class NewScene4 : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene(unsigned int tempScore);
	virtual bool init();
	CREATE_FUNC(NewScene4);
	void Again(Ref *pSender);
	void Menu(Ref *pSender);
	void End(Ref *pSender);
};

#endif // __NEW_SCENE_H__
