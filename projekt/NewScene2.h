#ifndef __NEW_SCENE2_H__
#define __NEW_SCENE2_H__

#include "cocos2d.h"

class NewScene2 : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene(unsigned int h1, unsigned int h2, unsigned int h3, unsigned int h4, unsigned int h5);
	virtual bool init();
	CREATE_FUNC(NewScene2);
	void Menu(Ref *pSender);
};

#endif // __NEW_SCENE_H__
