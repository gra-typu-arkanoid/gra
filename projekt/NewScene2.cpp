#include "NewScene2.h"
#include "NewScene.h"
#include "HelloWorldScene.h"

USING_NS_CC;
unsigned int score1, score2, score3, score4, score5;
Scene* NewScene2::createScene(unsigned int h1, unsigned int h2, unsigned int h3, unsigned int h4, unsigned int h5)
{
	score1 = h1;
	score2 = h2;
	score3 = h3;
	score4 = h4;
	score5 = h5;
	auto scene = Scene::create();
	auto layer = NewScene2::create();
	scene->addChild(layer);
	return scene;
}

bool NewScene2::init()
{

	if (!Scene::init())
	{
		return false;
	}

	Size visibleSize = Director::getInstance()->getVisibleSize();
	Point origin = Director::getInstance()->getVisibleOrigin();

	auto menu_item_1 = MenuItemFont::create("Powrot", CC_CALLBACK_1(NewScene2::Menu, this));

	
	{
		auto label = Label::createWithSystemFont(std::to_string(score1), "Arial", 20);
		label->setPosition(visibleSize.width /2, (visibleSize.height / 8) * 7);
		this->addChild(label);
	}
	{
		auto label = Label::createWithSystemFont(std::to_string(score2), "Arial", 20);
		label->setPosition(visibleSize.width /2, (visibleSize.height / 8) * 6);
		this->addChild(label);
	}
	{
		auto label = Label::createWithSystemFont(std::to_string(score3), "Arial", 20);
		label->setPosition(visibleSize.width /2, (visibleSize.height / 8) * 5);
		this->addChild(label);
	}
	{
		auto label = Label::createWithSystemFont(std::to_string(score4), "Arial", 20);
		label->setPosition(visibleSize.width /2, (visibleSize.height / 8) * 4);
		this->addChild(label);
	}
	{
		auto label = Label::createWithSystemFont(std::to_string(score5), "Arial", 20);
		label->setPosition(visibleSize.width /2, (visibleSize.height / 8) * 3);
		this->addChild(label);
	}
	menu_item_1->setPosition(Point(visibleSize.width / 2, (visibleSize.height / 8) *2 ));

	auto *menu = Menu::create(menu_item_1, NULL);
	menu->setPosition(Point(0, 0));
	this->addChild(menu);


	return true;
}

void NewScene2::Menu(cocos2d::Ref *pSender)
{
	Director::getInstance()->popScene();
}